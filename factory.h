#ifndef __HIVE_FACTORY
#define __HIVE_FACTORY

#include <Sodaq_BMP085.h>
#include <Sodaq_SHT2x.h>
#include <RTCTimer.h>
#include <Adafruit_AM2315.h>
#include <Adafruit_MLX90614.h>
#include "D6T_8L_06.h"

const uint8_t MLX_START_RANGE_I2CADDR = 0x78;
const byte NUMBER_OF_MLX_SENSORS = 7;

// TPH BMP sensor
const Sodaq_BMP085 bmp;

RTCTimer timer;

const Adafruit_AM2315 am2315;

const D6T_8L_06 d6t;

// MLX IR sensor
Adafruit_MLX90614 mlx[NUMBER_OF_MLX_SENSORS];

byte getNumberOfMlxSensors() {
  return NUMBER_OF_MLX_SENSORS;
}

void setupFactory() {
  for (int i = 0; i < getNumberOfMlxSensors(); i++) {
    mlx[i] = Adafruit_MLX90614(MLX_START_RANGE_I2CADDR + i);
  }
}

Sodaq_BMP085 getBmpSensor() {
  return bmp;
}

SHT2xClass getSht2xSensor() {
  return SHT2x;
}

RTCTimer getTimer() {
  return timer;
}

Adafruit_AM2315 getAm2315Sensor() {
  return am2315;
}

Adafruit_MLX90614 getMlx90614Sensor(byte number) {
  return mlx[number];
}

D6T_8L_06 getD6TSensor() {
  return d6t;
}

#endif
