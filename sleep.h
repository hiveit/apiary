#ifndef __HIVE_SLEEP
#define __HIVE_SLEEP

#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <Sodaq_PcInt.h>

//RTC Interrupt pin - this pin enables us to wake up the Mbili using the RTC timer as an external interrupt
#define RTC_PIN A7

//void enterSleep(void)
// {
//  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
//  sleep_enable();
//  sleep_mode();
//  /** The program will continue from here. **/
//  /* First thing to do is disable sleep. */
//  sleep_disable();
//  Serial.println("Waking up...");
// }
//

void wakeISR() {
  // intentionally left blank
}

void setupSleep() {

  pinMode(RTC_PIN, INPUT_PULLUP);
  PcInt::attachInterrupt(RTC_PIN, wakeISR);

  // Setup the RTC in interrupt mode
  rtc.begin();

  // Set the sleep mode
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
}

//######### watchdog and system sleep #############
void systemSleep(int sleepPeriodMillis) {
  Serial.print("Sleeping mode for ");
  Serial.print(sleepPeriodMillis / 1000);
  Serial.println(" seconds");

//Wait until the serial ports have finished transmitting
  Serial.flush();
  Serial1.flush();

//Schedule the next wake up pulse timeStamp + SLEEP_PERIOD
  DateTime wakeTime(getNow() + (sleepPeriodMillis / 1000));
  rtc.enableInterrupts(wakeTime.hour(), wakeTime.minute(), wakeTime.second());

//The next timed interrupt will not be sent until this is cleared
  rtc.clearINTStatus();

//Disable ADC
  ADCSRA &= ~_BV(ADEN);

//Sleep time
  noInterrupts();
  sleep_enable();
  interrupts();
  sleep_cpu();
  sleep_disable();

//Enbale ADC
  ADCSRA |= _BV(ADEN);

  Serial.println("Waking-up");

//This method handles any sensor specific wake setup
}
#endif
