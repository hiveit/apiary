#ifndef __HIVE_STORAGE
#define __HIVE_STORAGE

#include <SD.h>
#include "logging.h"

//Digital pin 11 is the MicroSD slave select pin on the Mbili
#define SD_SS_PIN 11

//The data log file
char* DATA_FOLDER = (char*) "/data";
char* DATA_FILE_PATH = (char*) "/data/data.csv";

//Data header
//MSCToDo - automate generation of onewire data header columns based on number discovered
#define DATA_HEADER "TimeDate,TempSHT21,TempBMP,PressureBMP,HumiditySHT21,BatteryLevelMVolts,TempAM2315,HumidityAM2315,AmbientTempMlx,ObjectTempMlx,TempD6T"

void setupLogFile() {

  //Initialise the SD card

  if (!SD.begin(SD_SS_PIN)) {
    error("Error: SD card failed to initialise or is missing.");
    //Hang
    while (true)
      ;
  }

  if (!SD.exists(DATA_FOLDER)) {

    debug("Creating data folder for sensor readings");

    SD.mkdir(DATA_FOLDER);

    debug(" - done");
  }
}

void storeData(String data) {

  if (!SD.exists(DATA_FILE_PATH)) {
    debug("Creating data file for sensor readings");

    File newDataFile = SD.open(DATA_FILE_PATH, FILE_WRITE);
    newDataFile.println(DATA_HEADER);
    newDataFile.close();

    debug(" - done");
  }

  // Re-open the file
  File logFile = SD.open(DATA_FILE_PATH, (FILE_WRITE | O_APPEND));

  // Write the CSV data
  logFile.println(data);

  // Close the file to save it
  logFile.close();
}

int getDataLineCount() {
  File logFile = SD.open(DATA_FILE_PATH);
  String data = logFile.readString();
  int lineCount = splitCount(data, "\n");
  logFile.close();
  return lineCount;
}

void getDataLines(String *dataBuffer) {
  File logFile = SD.open(DATA_FILE_PATH);
  String data = logFile.readString();
  split(data, "\n", dataBuffer);
  logFile.close();
}

void clearData() {
  debug("Clearing down stored data file");
  SD.remove(DATA_FILE_PATH);
  debug(" - done");
}

int writeData(String inputData) {

  if (isDebug()) {
    String message = "Storing data to SD card: ";
    message += inputData;
    debug(message);
  }

  storeData(inputData);

  debug(" - done");

  return getDataLineCount();
}

#endif
