#ifndef __HIVE_SOUND
#define __HIVE_SOUND

#include <SD.h>
#include <SPI.h>
#include "logging.h"

const int sampleWindow = 100; // Sample window width in milliseconds, obviously make sure that we aren't taking readings more often than this (unlikely)
const int micPin = 0;
unsigned int sample;


float getPeakToPeakAmplitude() {
  unsigned long startMillis = millis();
  unsigned int peakToPeakAmplitude = 0;
  unsigned int signalMax = 0;
  unsigned int signalMin = 1024;


  while (millis() - startMillis < sampleWindow) // use the Arduino's inbuilt millis function to time the sampling window - note that this function will overflow back to 0 roughly once every 50 days
  {
    sample = analogRead(micPin);

    if (sample < 1025) // ditch any readings above this as they are nonsense

    { 
      if (sample > signalMax)
      {
        signalMax = sample; // set new maximum amplitude level
      }
      else if (sample < signalMin)
      {
        signalMin = sample; // set new minimum amplitude level
      }
    }
  }

  if (signalMin == 1024 || signalMax == 0 || signalMin > signalMax) // if any of these conditions hold true, something is badly wrong with the circuit
  {
    warn("No readings found at all in the expected frequency range. Check that you have the correct pin and that your wiring is ok.");
    return 0;
  }

  peakToPeakAmplitude = signalMax - signalMin;
  double convertedSignal = (peakToPeakAmplitude * 3.3) /1024; // convert the signal to a more reasonable number scale

  if (convertedSignal > 1.0 && convertedSignal < 2.5) { // obviously these numbers may have to be changed once they've been tested with actual beasties
    // Serial.println("The hedgehogs are snuffling"); this is good for testing data, but not needed for the hog house
  }

  else if (convertedSignal >= 2.0) {                  // obviously these numbers may have to be changed once they've been tested with actual beasties
   // Serial.println("The hedgehogs are going crazy!"); this is good for testing data, but not needed for the hog house
  }

  return convertedSignal;
   


}



#endif
