#ifndef __HIVE_GPRS
#define __HIVE_GPRS

#include <GPRSbee.h>
#include "logging.h"

//Network constants
#define APN "giffgaff.com"
#define APN_USERNAME "giffgaff"
#define APN_PASSWORD ""

//SpeakThings constants
const String BASE_URL = "hivesensors.hiveit.co.uk";
const String ACCOUNT_ID = "561fc5e309000046008721fb";
const String BUCKET_ID = "561fc66809000038008721fc";

//#define WRITE_API_KEY "MA9YQHXNM3B6809W" //Change to your channel's key

//Seperators
#define FIRST_SEP "?"
#define OTHER_SEP "&"
#define LABEL_DATA_SEP "="

//Data labels, cannot change for ThingSpeak
#define LABEL1 "field1"
#define LABEL2 "field2"
#define LABEL3 "field3"
#define LABEL4 "field4"
#define LABEL5 "field5"
#define LABEL6 "field6"
#define LABEL7 "field7"

void setupComms()
{
  //Start Serial1 the Bee port
  Serial1.begin(9600);

  //Intialise the GPRSbee
  gprsbee.init(Serial1, BEECTS, BEEDTR);

  //uncomment this line to debug the GPRSbee with the serial monitor
  //gprsbee.setDiag(Serial);

  //This is required for the Switched Power method
  gprsbee.setPowerSwitchedOnOff(true);
}

bool sendJsonData(String jsonBody)
{
  //Construct data BASE_URL
  String url = BASE_URL + "/account/" + ACCOUNT_ID + "/bucket/" + BUCKET_ID + "/data";

  debug("Sending data to API server URL: " + url);

  String data = "dataAsJson=" + jsonBody;

  debug("TODO - reincorporate sending!!!");
//  gprsbee.doHTTPPOST(APN, APN_USERNAME, APN_PASSWORD, url.c_str(), jsonBody.c_str(), jsonBody.length());

  debug("Data sent");

  return true;
}

#endif
