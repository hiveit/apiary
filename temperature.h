#ifndef __HIVE_TEMPERATURE
#define __HIVE_TEMPERATURE

#include "factory.h"

void setupTemperatureSensors()
{
  //Initialise the wire protocol for the TPH sensors
  Wire.begin();

  //Initialise the TPH BMP sensor
  getBmpSensor().begin();
}

float getSht2xTemperature() {
	return getSht2xSensor().GetTemperature();
}

float getBmpTemperature() {
	return getBmpSensor().readTemperature();
}

float getAm2315Temperature() {
  return getAm2315Sensor().readTemperature();
}

float getMlxAmbientTemperature(byte sensorIndex) {

  return getMlx90614Sensor(sensorIndex).readAmbientTempC();
}

float getMlxObjectTemperature(byte sensorIndex) {

  return getMlx90614Sensor(sensorIndex).readObjectTempC();
}

float* getD6TTemperatures() {

  return getD6TSensor().readTemperaturesC();
}

byte getNumberOfD6TReadings() {
  return getD6TSensor().getNumberOfReadings();
}

#endif
