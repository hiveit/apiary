To use this code, configure an Arduino as per the diagram in the Pics folder, enter the desired target address in the NewMLXAddr variable in remap_mlx90614_i2c_address.ino, upload the sketch and wait to be told to "Cycle power NOW to set address".  Press the touch button to cycle the power and the program will converse with the sensor again on the new address.

NOTE THAT on the photos, the black blob in marker on the masking tape on the sensor shows the position of the "pip" on the MLX90614's casing that lets you know what pins do what!

i2cmaster.h library originally found at https://github.com/chrisramsay/arduino-projects/tree/master/libraries/I2Cmaster

Original tutorial for building this circuit and the complimentary code found at http://www.chrisramsay.co.uk/posts/2014/09/arduino-and-multiple-mlx90614-sensors/#giving-an-mlx90614-a-new-address
