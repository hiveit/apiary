#ifndef __HIVE_LOGGING
#define __HIVE_LOGGING

#include "utils.h"

const int TRACE = 0;
const int DEBUG = 1;
const int INFO = 2;
const int WARN = 3;
const int ERROR = 4;
const int NONE = 5;

const int loggingLevel = INFO;

bool isTrace() {
  return loggingLevel <= TRACE;
}

bool isDebug() {
  return loggingLevel <= DEBUG;
}

bool isInfo() {
  return loggingLevel <= INFO;
}

bool isWarn() {
  return loggingLevel <= WARN;
}

bool isError() {
  return loggingLevel <= ERROR;
}

bool isNone() {
  return loggingLevel == NONE;
}

void printMessage(String mode, String message) {

  String finalMessage = getNowString();
  finalMessage += " ";
  finalMessage += mode;
  finalMessage += " - ";
  finalMessage += message;

  Serial.println(finalMessage);
}

void trace(String message) {
  if (isTrace()) printMessage("TRACE", message);
}

void debug(String message) {
  if (isDebug()) printMessage("DEBUG", message);
}

void info(String message) {
  if (isInfo()) printMessage("INFO", message);
}

void warn(String message) {
  if (isWarn()) printMessage("WARN", message);
}

void error(String message) {
  if (isError()) printMessage("ERROR", message);
}

#endif
