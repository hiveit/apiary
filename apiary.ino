#include "gprs.h"
#include "utils.h"
#include "storage.h"
#include "scheduler.h"
#include "temperature.h"
#include "battery.h"
#include "humidity.h"
#include "pressure.h"
#include "D6T_8L_06.h"
#include "sound.h"
#include "sleep.h"
#include "logging.h"

#include <GPRSbee.h>
#include <RTCTimer.h>
#include <SD.h>
#include <Sodaq_BMP085.h>
#include <Sodaq_SHT2x.h>
#include <Sodaq_DS3231.h>
#include <Sodaq_PcInt.h>


// Removed 1 wire scan and display code, see 001 if needed
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>

#include <Adafruit_MLX90614.h>
#include <Adafruit_AM2315.h>

const byte NUMBER_OF_STORED_ENTRIES_BEFORE_SEND = 5;
const int LONGER_DELAY = 30000;
const int NORMAL_DELAY = 100;
const byte SLEEP_DELAY_TEMP_THRESHOLD = 50;

void setup()
{
  //Initialise the serial connection
  Serial.begin(115200);

  info("Welcome to aPIary!");
  info("==================");
  info("");

  // initialise the configuration factory
  info("  - Configuring system...");
  setupFactory();

  //Initialise sensors
  info("  - Setting up sensors...");
  setupSensors();

  //Initialise log file
  info("  - Setting up log file...");
  setupLogFile();
  info("Ready!");
  setupSleep();
  
}

void loop() {

  takeReading(getNow());
  systemSleep(LONGER_DELAY);
  //  int my_delay = tempCheck();
  // delay(3000);
}

bool sendData(String data) {

  debug("Sending stored data: " + data);

  bool dataSent = sendJsonData(data);

  if (isDebug()) {
    String message = "Finished sending stored data ";
    message += (dataSent ? "successfully" : "unsuccessfully");
    debug(message);
  }
  return dataSent;
}


// If any of the D6T pixel cameras go over 50C, the Arduino slows down its sampling by a factor of 10, until the D6T cools down again.
// Of course in the wild we'll want this to happen at a low temp, rather than a high temp
// but this way is easier to test with a cup of coffee!

int tempCheck() {
  for (int i = 1; i < 8; i++) {
    float nextTemp = getD6TTemperatures()[i];
    if (nextTemp >= SLEEP_DELAY_TEMP_THRESHOLD) {
      return LONGER_DELAY;
    }
  }
  return NORMAL_DELAY;
}

String createDataRecord(float sht2xTemp, float bmpTemp, float bmpPressure, float sht2xHumidity, float batteryLevel, float am2315Temperature, float am2315Humidity, float (*mlxAmbientTemperatureGetter)(byte sensorIndex), float (*mlxObjectTemperatureGetter)(byte sensorIndex), byte numberOfMlxSensors, float* d6TTemperatures, byte numberOfD6TReadings, float peakToPeakAmplitude)
{
  String data = String(getNow());
  data += ",";
  data += String(sht2xTemp)  + ",";
  data += String(bmpTemp) + ",";
  data += String(bmpPressure) + ",";
  data += String(sht2xHumidity) + ",";
  data += String(batteryLevel) + ",";
  data += String(am2315Temperature) + ",";
  data += String(am2315Humidity) + ",";

  for (int i = 0; i < numberOfMlxSensors; i++) {
    data += String(mlxAmbientTemperatureGetter(i)) + (i < numberOfMlxSensors - 1 ? "|" : "");
  }

  data += ",";

  for (int i = 0; i < numberOfMlxSensors; i++) {
    data += String(mlxObjectTemperatureGetter(i)) + (i < numberOfMlxSensors - 1 ? "|" : "");
  }

  data += ",";

  for (int i = 0; i < numberOfD6TReadings; i++) {
    data += String(d6TTemperatures[i]) + (i < numberOfD6TReadings - 1? "|" : "");
  }

  data += ", ";

  data += String(peakToPeakAmplitude);

  return data;
}

void takeReading(uint32_t ts)
{
  debug("Taking readings of all sensors and writing to sd card now...");
  
  //Create the data record
  String dataRec = createDataRecord(getSht2xTemperature(), getBmpTemperature(), getBmpPressure(), getSht2xHumidity(), getBatteryLevel(), getAm2315Temperature(), getAm2315Humidity(), getMlxAmbientTemperature, getMlxObjectTemperature, getNumberOfMlxSensors(), getD6TTemperatures(), getNumberOfD6TReadings(), getPeakToPeakAmplitude());

  if (isInfo()) {
    String message = "Readings were: ";
    message += dataRec;
    info(message);
  }

  // Write the data to the SD card, and retrieve it after five entries
  int numberOfStoredLines = writeData(dataRec);
 
  debug(numberOfStoredLines + " stored lines now on SD card");
  
  if (numberOfStoredLines - 1 >= NUMBER_OF_STORED_ENTRIES_BEFORE_SEND) {

    debug(numberOfStoredLines + " exceeds threshold of number to store before sending to API server - going to try sending data now");

    String data[numberOfStoredLines];
    getDataLines(data);

    String dataJson = dataAsJson(data, numberOfStoredLines - 1);
    bool dataSent = sendData(dataJson);

    if (dataSent) {

      debug("Data was sent successfully, so clearing SD card file now");
      clearData();
      debug("SD card file cleared successfully");
    }
  }
}

String quote(String unquoted) {
  return "\"" + unquoted + "\"";
}

String nvp(String name, String value, bool trailingComma = true, bool quoteValue = true) {
  return quote(name) + ":" + (quoteValue ? quote(value) : value) + (trailingComma ? "," : "");
}

String dataAsJson(String dataLinesAsCsvs[], int numberOfLinesOfData) {

  int numberOfHeaders = splitCount(dataLinesAsCsvs[0], ",");
  String headers[numberOfHeaders];
  split(dataLinesAsCsvs[0], ",", headers);

  String json = "{data:[";

  for (int i = 1; i < numberOfLinesOfData; i++) {

    String dataLineAsCsv = dataLinesAsCsvs[i];
    int numberOfDataValues = splitCount(dataLineAsCsv, ",");
    String dataValues[numberOfDataValues];
    split(dataLineAsCsv, ",", dataValues);

    json += "{";

    json += nvp("recordedTime", dataValues[0]);

    String headerDataPairsAsJsonArray = "[";

    for (int j = 1; j < numberOfHeaders; j++) {

      String dataValuesPipeSeparated = dataValues[j];
      int numberOfIndividualDataValues = splitCount(dataValuesPipeSeparated, "|");
      String individualDataValues[numberOfIndividualDataValues];
      split(dataValuesPipeSeparated, "|", individualDataValues);

      String individualDataValuesAsJsonArray = "[";

      for (int k = 0; k < numberOfIndividualDataValues; k++) {
        individualDataValuesAsJsonArray += quote(individualDataValues[k]) + (k < numberOfIndividualDataValues - 1 ? "," : "");
      }

      individualDataValuesAsJsonArray += "]";

      bool addTrailingComma = j < numberOfHeaders - 1;
      String headerDataPair = "{" + nvp("header", headers[j]) + nvp("values", individualDataValuesAsJsonArray, false, false) + "}";
      headerDataPairsAsJsonArray += headerDataPair + (addTrailingComma ? "," : "");
    }

    headerDataPairsAsJsonArray += "]";

    json += nvp("headerDataPairs", headerDataPairsAsJsonArray, false, false);

    json += "}";

    json += (i < numberOfLinesOfData - 1 ? "," : "");
  }

  json += "]}";

  return json;
}

void setupSensors()
{
  setupTemperatureSensors();

  //Setup GPRSbee
  setupComms();

  setupPressureSensors();

  setupHumiditySensors();

  setupBatteryLevels();
}
