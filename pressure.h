#ifndef __HIVE_PRESSURE
#define __HIVE_PRESSURE

#include "factory.h"

void setupPressureSensors()
{
  //Initialise the wire protocol for the TPH sensors
  Wire.begin();
}

float getBmpPressure() {
  return getBmpSensor().readPressure() / 100;
}

#endif
