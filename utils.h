#ifndef __HIVE_UTILS
#define __HIVE_UTILS

#include <RTCTimer.h>
#include <Sodaq_DS3231.h>

uint32_t getNow()
{
  return rtc.now().getEpoch();
}

String getNowString()
{
  DateTime now = rtc.now();

  String timeString = String(now.date());
  timeString += "/";
  timeString += String(now.month());
  timeString += "/";
  timeString += String(now.year());
  timeString+= " ";
  timeString += String(now.hour());
  timeString += ":";
  timeString += String(now.minute());
  timeString += ":";
  timeString += String(now.second());

  return timeString;
}

String getDateTime()
{
  String dateTimeStr;

  //Create a DateTime object from the current time
  DateTime dt(rtc.makeDateTime(rtc.now().getEpoch()));

  //Convert it to a String
  dt.addToString(dateTimeStr);

  return dateTimeStr;
}

int splitCount(String stringToSplit, String delimiter) {

  if (stringToSplit.indexOf(delimiter) == -1) {
    return 1;
  }

  int previousLineBreak = -1;

  int lineCount = 0;
  while (true) {
    int nextLineBreak = stringToSplit.indexOf(delimiter, previousLineBreak + 1);
    if (nextLineBreak == -1) {
      if (!stringToSplit.endsWith(delimiter)) {
        lineCount ++;
      }
      break;
    }
    lineCount ++;
    previousLineBreak = nextLineBreak;
  }

  return lineCount;
}

void split(String stringToSplit, String delimiter, String* dataBuffer) {

  if (stringToSplit.indexOf(delimiter) == -1) {
    dataBuffer[0] = stringToSplit;
    return;
  }

  int previousLineBreak = -1;

  int lineCount = 0;
  while (true) {

    int nextLineBreak = stringToSplit.indexOf(delimiter, previousLineBreak + 1);

    if (nextLineBreak == -1) {
      if (!stringToSplit.endsWith(delimiter)) {
        String nextLine = stringToSplit.substring(previousLineBreak + 1, stringToSplit.length() - 1);
        dataBuffer[lineCount ++] = nextLine;
      }
      break;
    } else {
      String nextLine = stringToSplit.substring(previousLineBreak + 1, nextLineBreak);
      dataBuffer[lineCount ++] = nextLine;
      previousLineBreak = nextLineBreak;
    }
  }
}

#endif
