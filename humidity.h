#ifndef __HIVE_HUMIDITY
#define __HIVE_HUMIDITY

#include "factory.h"

void setupHumiditySensors()
{
  //Initialise the wire protocol for the TPH sensors
  Wire.begin();
}

float getSht2xHumidity() {
  return getSht2xSensor().GetHumidity();
}

float getAm2315Humidity() {
  return getAm2315Sensor().readHumidity();
}

#endif
