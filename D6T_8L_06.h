#ifndef __HIVE_D6T
#define __HIVE_D6T

#include <Wire.h>

const uint8_t D6T_8L_06_I2CADDR = 0x0A;

const byte COMMAND = 0x4C;// Standard command
const byte NUMBER_OF_READINGS = 8; // number of temperature readings that the sensor returns
const byte NUMBER_OF_BYTES_TO_READ = (2 * (NUMBER_OF_READINGS + 1)) + 1; // Data length read from sensor

class D6T_8L_06  {
 public:
  D6T_8L_06(uint8_t addr = D6T_8L_06_I2CADDR, boolean verbose = false);
  void begin(void);
  float* readTemperaturesC(void);
  byte getNumberOfReadings(void);
 private:
  uint8_t _addr;
  uint8_t _verbose;
  float *_readings;
};

///////// PUBLIC METHODS /////////////////////////////

D6T_8L_06::D6T_8L_06(uint8_t i2caddr, boolean verbose) {
  _addr = i2caddr;
  _verbose = verbose;
  _readings = new float[NUMBER_OF_READINGS];
}

void D6T_8L_06::begin() {

  Wire.begin();
}

byte D6T_8L_06::getNumberOfReadings() {
  return NUMBER_OF_READINGS;
}

float* D6T_8L_06::readTemperaturesC() {

  // get sensor data
  Wire.beginTransmission(_addr); // start send write (14h)
  Wire.write(COMMAND); // send command byte
  Wire.endTransmission();
  delay(1);
  Wire.requestFrom(_addr, NUMBER_OF_BYTES_TO_READ);

  byte data[NUMBER_OF_BYTES_TO_READ];

  for(byte i = 0; i < NUMBER_OF_BYTES_TO_READ; i++) {
    data[i] = Wire.read(); // read char 0-18 (2*(8+1)+1=19)
  }

  Wire.endTransmission(); // stop

  // Get values
  uint16_t ptat = 0xFF * data[1] + data[0]; // reference temperature 10 times value of deg C

  for (byte i = 0; i < NUMBER_OF_READINGS; i++) {
    uint16_t celsiusX10 = 0xFF * (unsigned int) data[((i + 1) * 2) + 1] + (unsigned int) data[(i + 1) * 2];
    _readings[i] = celsiusX10 / 10;
  }

  if (_verbose) {

    byte packetErrorCheck = data[NUMBER_OF_BYTES_TO_READ - 1]; // packet error check code based on the "sm bus" specification

    // print sensor data
    Serial.print("Temp (1=0.1grad): ");
    Serial.print(ptat,DEC);
    Serial.print(", ");
    for (byte i = 0; i < (byte) NUMBER_OF_READINGS; i++) {
      Serial.print("P");
      Serial.print(i,DEC);
      Serial.print(": ");
      Serial.print(_readings[i],DEC);
      Serial.print(", ");
    }
    Serial.print("Check: ");
    Serial.println(packetErrorCheck,HEX);
  }

  return _readings;
}

///////// PRIVATE METHODS ////////////////////////////

#endif
