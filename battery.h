#ifndef __HIVE_BATTERY
#define __HIVE_BATTERY

#include <Wire.h>

//These constants are used for reading the battery voltage
#define ADC_AREF 3.3
#define VOLTAGE_FROM_BATTERY_PIN A6
#define BATTERY_VOLTAGE_READING_CIRCUIT_RESISTOR_1 4.7
#define BATTERY_VOLTAGE_READING_CIRCUIT_RESISTOR_2 10

void setupBatteryLevels()
{
  //Start the I2C protocol
  Wire.begin();
}

float getRealBatteryVoltage()
{
  uint16_t batteryVoltage = analogRead(VOLTAGE_FROM_BATTERY_PIN);
  return (ADC_AREF / 1023.0) * (BATTERY_VOLTAGE_READING_CIRCUIT_RESISTOR_1 + BATTERY_VOLTAGE_READING_CIRCUIT_RESISTOR_2) / BATTERY_VOLTAGE_READING_CIRCUIT_RESISTOR_2 * batteryVoltage;
}

float getBatteryLevel()
{
	int millivolts = getRealBatteryVoltage() * 1000.0;
	return millivolts;
}

#endif
