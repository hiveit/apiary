#include <SD.h>
#include <SPI.h>


const int sampleWindow = 100; // Sample window width in milliseconds
const int cardPin = 10; // correct pin for the arduino data logger shield, check that this matches your set up!
const int micPin = 4; // check this one too...
unsigned int samplingRate;
unsigned int highestUsefulFrequency;
unsigned int sample;


void setup() 
{
   Serial.begin(115200);
   Serial.println("Setting up the SD card...");

   if  (!SD.begin(cardPin)) {
    Serial.println("SD card failure. Is the card definitely present and is the pin correct?");  // basic check that the SD card exists
    return;
   }

   Serial.println("SD card initialised");
   
}
 
 
void loop() 
{
   unsigned long startMillis= millis();
   unsigned int peakToPeak = 0;
   unsigned int signalMax = 0;
   unsigned int signalMin = 1024;
   unsigned int samplingCounter = 0;


   while (millis() - startMillis < sampleWindow)  //use the inbuilt Arduino millis() function to keep track of time for the sampling window (note that millis() overflows back to 0 roughly once every 50 days)
   {
      sample = analogRead(micPin);

      samplingCounter++;
      
      if (sample < 1025) // any reading higher than this is nonsense
      {
         if (sample > signalMax)
         {
            signalMax = sample;  // set new max amplitude level
         }
         else if (sample < signalMin)
         {
            signalMin = sample;  // set new min amplitude level
         }
      }
   }
   if (signalMin == 1024 || signalMax == 0 || signalMin > signalMax) {
    Serial.println("There don't appear to be any readings at all in the expected frequency range. Something is broken!");
    return;
   }
   peakToPeak = signalMax - signalMin;  // 
   double convertedSignal = (peakToPeak * 3.3) / 1024;  // convert to a useful level

   samplingRate = (samplingCounter / sampleWindow) * 1000; // number of times the Arduino samples per second during its sampling window

   highestUsefulFrequency = (0.5 * samplingRate); // look up the Nyquist sampling theorem for an explanation of this rule and a headache 



   if (convertedSignal < 1.0 && convertedSignal >= 0.3) {
    Serial.println("The hedgehogs are snuffling"); // currently only storing the data for larger amounts of noise on the SD card, but noting this for testing purposes
                                                    // of course, all this is subject to change once we try it out with some actual beasties

   } else if (convertedSignal >= 1.0) {
    Serial.println("The hedgehogs are going mental!");
    
    File dataFile = SD.open("datalog.txt", FILE_WRITE);

     if (dataFile) {
       dataFile.print("The hedgehogs were making some noise, here's the peak-to-peak amplitude reading: ");
       dataFile.print(convertedSignal);
       dataFile.print(" and the highest usable frequency from that data reading is ");
       dataFile.print(highestUsefulFrequency);
       dataFile.close();
     } else {
       Serial.println("error opening datalog.txt");
       }

  }
  Serial.println(convertedSignal);
   
  delay(3000);
}
