#!/usr/bin/python

import serial
import sys

# Make sure the script is being run with the necessary arguments

if len(sys.argv) < 3:
  print 'Script usage: ./arduino.py <arduino_port> <output_text_file>'
  exit(0)

ser = serial.Serial(sys.argv[1], 9600) # this often changes, if the script is failing check Arduino IDE to make sure that the Arduino port and baud rate matches here
f = open(sys.argv[2], 'w')


# when the script starts up, the first reading or two is often mangled. We'll skip the first five readings to make sure that none of the mangled readings end up in our data
i = 0
while i < 5:
  junk = ser.readline()
  i += 1

# now we capture one hundred data points and print each to the console for a sanity check

j = 0
while j < 100:
  datum = ser.readline()
  print datum
  f.write(datum)
  j += 1
  


f.close()





