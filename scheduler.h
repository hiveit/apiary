#ifndef __HIVE_SCHEDULER
#define __HIVE_SCHEDULER

#include "factory.h"

// delay in between reads in ms
//  1 secs   - 1000
//  5 secs   - 5000
//  10 secs  - 10000
//  30 secs  - 30000
//  1 min    - 60000
//  5 mins   - 300000
//  10 mins  - 600000
//  15 mins  - 900000
//  30 mins  - 1800000
//  60 mins  - 3600000
RTCTimer theTimer = getTimer();

void setupTimer(uint32_t (*now)(), void (*callback)(uint32_t now), int intervalInMillis)
{
  //Initialise the DS3231 RTC
  rtc.begin();

  //Instruct the RTCTimer how to get the current time reading
  theTimer.setNowCallback(now);

  //Schedule the reading every second
  theTimer.every(intervalInMillis / 1000, callback);
}

void timerTick() {

  //Update the timer
  theTimer.update();
}

#endif
